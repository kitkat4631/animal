
//  https://eli.thegreenplace.net/2013/10/22/classical-inheritance-in-javascript-es5


//-- D'abord, les classes

function Vehicule(poids, vitesse) {   // le constructeur  
    this.poids = poids; // son poids  
    this.vitesse = vitesse; // sa vitesse  

    this.avancer = function () { // sa méthode pour avancer  
        console.log("vroum vroum");
    }
    this.stopper = function(){}
}


function Voiture(poids, vitesse, nombre_passagers) {
    Vehicule.call(this, poids, vitesse);    //   <- éq. SUPER (appeler le constructeur parent)

    this.nombre_passagers = nombre_passagers;

    this.allume_autoradio = function () { // méthode pour allumer l'auto-radio  
        console.log("lalala");
    }
}
Voiture.prototype = new Vehicule();  // Heritage
Voiture.prototype.constructor = Voiture;  // Tres important : on definit le constructeur pour pouvoir le modifier


//-- Le programme MAINTENANT

var v0 = new Vehicule(700, 4);
var v1 = new Voiture(1400, 6, 5);
var v2 = new Voiture(1200, 5, 5);

v0.avancer();
v2.allume_autoradio();
v2.avancer();
console.log( v2.poids );

